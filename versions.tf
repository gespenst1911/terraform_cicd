terraform {
  backend "s3" {
    bucket                      = "terraform-state-devops"
    key                         = "terraform.tfstate"
    region                      = "fr-par"
    endpoint                    = "https://s3.fr-par.scw.cloud"
    access_key                  = "SCWSYC4G9ABTV80E10N8"
    secret_key                  = "ae5ef57b-1389-4f6b-aee4-a0b0f515b61f"
    skip_credentials_validation = true
    skip_region_validation      = true
  }
  required_providers {
    local = {
      source = "hashicorp/local"
    }
    scaleway = {
      source = "terraform-providers/scaleway"
    }
  }
  required_version = ">= 0.13"
}
