provider "scaleway" {
  version = "1.13"
  organization_id = var.organization_id
  region          = "fr-par"
  zone            = "fr-par-1"
}

resource "scaleway_instance_security_group" "sg-devops-public" {
  name = "sg-devops-public"
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"
  inbound_rule {
    action = "accept"
    protocol = "TCP"
    port = 22
    ip_range = "0.0.0.0/0"
  }
}


resource "scaleway_instance_security_group" "sg-devops-app" {
  name = "sg-devops-app"
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"
  inbound_rule {
    action = "accept"
    protocol = "TCP"
    port = 22
    ip_range = "0.0.0.0/0"
  }
  inbound_rule {
    action = "accept"
    protocol = "TCP"
    port = 80
    ip_range = "0.0.0.0/0"
  }
  inbound_rule {
    action = "accept"
    protocol = "TCP"
    port = 8081
    ip_range = "0.0.0.0/0"
  }
}

resource "scaleway_instance_security_group" "sg-devops-db" {
  name = "sg-devops-db"
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"
  inbound_rule {
    action = "accept"
    protocol = "TCP"
    port = 22
    ip_range = "0.0.0.0/0"
  }
  inbound_rule {
    action = "accept"
    protocol = "TCP"
    port = 6379
    ip_range = "${scaleway_instance_ip.app_ip.address}/32" # "0.0.0.0/0" # changer cette ip pour app
  }
}

resource "scaleway_instance_ip" "app_ip" {}
resource "scaleway_instance_server" "app" {
  type  = "DEV1-S"
  image = "centos_7.6"
  name  = "app"

  ip_id = scaleway_instance_ip.app_ip.id

  # security_group_id = scaleway_instance_security_group.sg-devops-public.id
  security_group_id = scaleway_instance_security_group.sg-devops-app.id
}

resource "scaleway_instance_ip" "db_ip" {}
resource "scaleway_instance_server" "db" {
  type  = "DEV1-S"
  image = "centos_7.6"
  name  = "db"

  ip_id = scaleway_instance_ip.db_ip.id

  # security_group_id = scaleway_instance_security_group.sg-devops-public.id
  security_group_id = scaleway_instance_security_group.sg-devops-db.id
}

resource "scaleway_account_ssh_key" "main" {
  name        = "main"
  public_key = var.public_key
}

resource "scaleway_account_ssh_key" "public_key_prof" {
  name        = "public_key_prof"
  public_key = var.public_key_prof
}