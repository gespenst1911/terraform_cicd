variable "organization_id" {
  type = string
  default="ea4f73e8-9728-4beb-b5bc-89965f614003"
}

variable "public_key" {
  type = string
  default="ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIALON0mLJN8TdcsMv3HiF1YSJovHXoOFmyWXuY51VmKO terraform_scaleway"
}

variable "public_key_prof" {
  type = string
  default="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCXYspeoz1bIbU7j59hvaYrjDGQbOs9vql/RFZk0PEtkUGZFodatKk65PpLL2w0x2iEICbacQ2ljDcaGa+isBgWG0iq6M6rM6x0H8wK7uj6cVktnJEMcJ1ALK4dHj4qyDzIOHcIi2y9kKm1Mz4hqdu35kE81Kj8XmA2bx/SUfPQm4c5+hWDTn7Gp6cgmrOyKYCWI5zR4CehV0xKCeyhdezkZ2PZSLz8aA3c8kUUWKhv4Z4dnP82UHexJK3zaFn06UjXLms0v7/hVfqcmpgfojeS0Ql4FpiuFxfWxi/YVzstJBTKa2NAIjEKGwF0WhUX2K0bahN1FRlFuVk4sDVnhgUL franck.cussac@gmail.com"
}
